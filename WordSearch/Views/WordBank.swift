import UIKit

class WordBank: UIView {
    private var searchWord: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        
        let bottom = UIView(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: frame.width,
            height: frame.height
        ))
    
        bottom.layer.borderColor = UIColor(hexString: "#37966f").cgColor
        bottom.layer.borderWidth = 2.0
        bottom.layer.cornerRadius = 4.0
        
        let maskView = UIView(frame: CGRect(
            x: 0.0,
            y: frame.height / 2.0,
            width: frame.width,
            height: frame.height / 2.0
        ))
        
        maskView.backgroundColor = .blue
        
        bottom.mask = maskView
                
        addSubview(bottom)
        
        let label = UILabel(frame: CGRect.zero)
        
        label.text = "Word Bank"
        label.font = UIFont(name: "Noteworthy-Bold", size: 20.0)
        label.textColor = UIColor(hexString: "#37966f")
        label.sizeToFit()
        label.center = CGPoint(x: frame.width / 2.0, y: 0.0)
        
        addSubview(label)
        
        let topLeft = UIView(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: frame.width,
            height: frame.height
        ))
           
        topLeft.layer.borderColor = UIColor(hexString: "#37966f").cgColor
        topLeft.layer.borderWidth = 2.0
        topLeft.layer.cornerRadius = 4.0
               
        let topLeftMask = UIView(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: frame.width / 2.0 - (label.frame.width / 2.0) - 8.0,
            height: frame.height / 2.0
        ))
               
        topLeftMask.backgroundColor = .blue
               
        topLeft.mask = topLeftMask
        
        addSubview(topLeft)
        
        let topRight = UIView(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: frame.width,
            height: frame.height
        ))
           
        topRight.layer.borderColor = UIColor(hexString: "#37966f").cgColor
        topRight.layer.borderWidth = 2.0
        topRight.layer.cornerRadius = 4.0
               
        let topRightMask = UIView(frame: CGRect(
           x: frame.width / 2.0 + (label.frame.width / 2.0) + 8.0,
           y: 0.0,
           width: frame.width / 2.0 - (label.frame.width / 2.0) - 8.0,
           height: frame.height / 2.0
        ))
               
        topRightMask.backgroundColor = .blue
               
        topRight.mask = topRightMask
        
        addSubview(topRight)
    }
    
    func setWord(
        _ str: String,
        fadeOutTime: TimeInterval = 0.0,
        fadeInDelay: TimeInterval = 0.0,
        fadeInTime: TimeInterval = 0.0
    ) {
        UIView.animate(withDuration: fadeOutTime, animations: {[weak self] in
            self?.searchWord?.alpha = 0.0
        }, completion: {[weak self] _ in
            guard let ss = self else { return }
            ss.searchWord?.removeFromSuperview()
            ss.searchWord = UILabel(frame: CGRect.zero)
            ss.searchWord.alpha = 0.0
            ss.searchWord.text = str
            ss.searchWord.font = UIFont(
                name: "AvenirNextCondensed-MediumItalic",
                size: 18.0
            )
            ss.searchWord.textColor = .black
            ss.searchWord.sizeToFit()
            ss.searchWord.center = CGPoint(
                x: ss.frame.width / 2.0,
                y: ss.frame.height / 2.0
            )
        
            ss.addSubview(ss.searchWord)
            
            UIView.animate(
                withDuration: fadeInTime,
                delay: fadeInDelay,
                animations: {
                self?.searchWord?.alpha = 1.0
            })
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
