import UIKit

class SelectView: UIView {
    init(color: UIColor, size: CGSize) {
        super.init(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: size.width,
            height: size.height
        ))
    
        backgroundColor = color
        
        layer.cornerRadius = size.width / 2.0
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
