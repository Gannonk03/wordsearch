import UIKit
import SpriteKit

enum SelectType {
    case Right
    case Left
    case Up
    case Down
    case DiagonalUpRight
    case DiagonalDownLeft
    case DiagonalUpLeft
    case DiagonalDownRight
    case None
    
    var isDiagonal: Bool {
        switch self {
        case .DiagonalUpRight, .DiagonalUpLeft, .DiagonalDownLeft, .DiagonalDownRight:
            return true
        default:
            return false
        }
    }
}

class SelectNode {
    private(set) var node: SKSpriteNode
    
    var selectType: SelectType = .None
    var coor: BoardCoordinate = BoardCoordinate(row: -1, col: -1)
    var position: CGPoint {
        get {
            return node.position
        }
        set(newPosition) {
            node.position = newPosition
        }
    }
    
    init(texture: SKTexture) {
        node = SKSpriteNode(texture: texture)
        
        let xy = (texture.size().width - 1.0) / 2.0
        
        node.centerRect = CGRect(
            x: xy / texture.size().width,
            y: xy / texture.size().width,
            width: 1.0 / texture.size().width,
            height: 1.0 / texture.size().width
        )
    }
}
