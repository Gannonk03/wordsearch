import UIKit
import SpriteKit

class LevelNode: SKNode {
    var nodes: Dictionary<BoardCoordinate,LetterNode> = [:]
    var boardData: GameBoardData
    var level: Level
    var board: SKSpriteNode
    var select: SelectNode
    var selectStartLoc: CGPoint = .zero
    
    init(_ level: Level) {
        self.boardData = level.boardData
        self.level = level
        self.select = SelectNode(texture: boardData.selectTexture)
        self.select.position = CGPoint(x: .min, y: .min)
            
        let bounds = UIScreen.main.bounds
        
        board = SKSpriteNode()
                        
        super.init()
        
        for row in 0..<level.dimension {
            for col in 0..<level.dimension {
                let coordinate = BoardCoordinate(row: row, col: col)
                let tile = createLetterNode(
                    coordinate,
                    level.board.characterGrid[row][col]
                )
                nodes[coordinate] = tile
                board.addChild(tile.node)
            }
        }
        
        let zeroX = bounds.size.width / 2.0
            - boardData.boardWidth / 2.0
            + boardData.tileWidth / 2.0
        
        let zeroY = bounds.size.height / 2.0
            - boardData.boardWidth / 2.0
            + boardData.tileWidth / 2.0
        
        board.position = CGPoint(x: zeroX,y: zeroY)
        board.alpha = 0.0
            
        addChild(board)
        
        board.zPosition = 2.0
    }
    
    func startSelection(at coor: BoardCoordinate) {
        guard select.node.parent == nil else { return }
        
        selectStartLoc = getTilePosition(coor)
        
        select.position = CGPoint(
            x: selectStartLoc.x - (boardData.tileWidth / 2),
            y: selectStartLoc.y
        )
        
        select.node.anchorPoint = CGPoint(x: 0.0, y: 0.5)
        
        select.coor = coor
        
        select.node.xScale = 1.5
        
        board.addChild(select.node)
    }
    
    func updateSelection(translation: CGPoint) {
        let center = CGPoint(
            x: translation.x - selectStartLoc.x,
            y: translation.y - selectStartLoc.y
        )
        
        let radians = atan2(center.y, center.x)
        
        var degrees = radians * 180.0 / .pi
        
        degrees = degrees > 0.0 ? degrees : degrees + 360.0
        
        let fatFingerOffset: Float = 50.0
        let leftRightMin = min(min(abs(degrees - 0.0),(abs(degrees - 360))), abs(degrees - 180.0))
        let upDownMin = min(abs(degrees - 90.0), abs(degrees - 270.0))
        let diagonalRight = min(abs(degrees - 45.0), abs(degrees - 225.0))
        let diagonalLeft = min(abs(degrees - 135.0), abs(degrees - 315.0))
        let dist = hypotf(
            Float(translation.x - selectStartLoc.x),
            Float(translation.y - selectStartLoc.y)
        )
        let direction = min(
            min(leftRightMin,upDownMin),
            min(diagonalLeft,diagonalRight)
        )
        
        var scale = CGFloat(dist + fatFingerOffset) / (boardData.tileWidth)
        var anchorPoint: CGPoint = .zero
        var position: CGPoint = .zero
        var selectType: SelectType = .None
                
        if leftRightMin == direction {
            if
                abs(degrees - 0.0) < abs(degrees - 180.0) ||
                abs(degrees - 360.0) < abs(degrees - 180.0) {
                selectType = .Right
                anchorPoint = CGPoint(x: 0.0, y: 0.5)
                position = CGPoint(
                    x: selectStartLoc.x - (boardData.tileWidth / 2),
                    y: selectStartLoc.y
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { $0.coor.col < $1.coor.col })
                
                if let first = selected.first {
                    let maxScale = level.dimension - first.coor.col
                    scale = min(scale,CGFloat(maxScale))
                }
            }
            else {
                selectType = .Left
                anchorPoint = CGPoint(x: 1.0, y: 0.5)
                position = CGPoint(
                    x: selectStartLoc.x + (boardData.tileWidth / 2),
                    y: selectStartLoc.y
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { $0.coor.col > $1.coor.col })
                
                if let first = selected.first {
                    let maxScale = first.coor.col + 1
                    scale = min(scale,CGFloat(maxScale))
                }
            }

            select.selectType = selectType
            select.node.zRotation = 0.0
            select.node.yScale = 1.0
            select.node.xScale = max(1.5,scale)
            select.node.anchorPoint = anchorPoint
            select.position = position
        }
        else if upDownMin == direction {
            if abs(degrees - 90.0) < abs(degrees - 270.0) {
                selectType = .Up
                anchorPoint = CGPoint(x: 0.5, y: 0.0)
                position = CGPoint(
                    x: selectStartLoc.x,
                    y: selectStartLoc.y - (boardData.tileWidth / 2)
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { return $0.coor.row < $1.coor.row })
                
                if let first = selected.first {
                    let maxScale = level.dimension - first.coor.row
                    scale = min(scale,CGFloat(maxScale))
                }
            }
            else {
                selectType = .Down
                anchorPoint = CGPoint(x: 0.5, y: 1.0)
                position = CGPoint(
                    x: selectStartLoc.x,
                    y: selectStartLoc.y + (boardData.tileWidth / 2)
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { return $0.coor.row > $1.coor.row })
                
                if let first = selected.first {
                    let maxScale = first.coor.row + 1
                    scale = min(scale,CGFloat(maxScale))
                }
            }

            select.selectType = selectType
            select.node.zRotation = 0.0
            select.node.xScale = 1.0
            select.node.yScale = max(1.5,scale)
            select.node.anchorPoint = anchorPoint
            select.position = position
        }
        else if diagonalRight == direction {
            if abs(degrees - 45.0) < abs(degrees - 225.0) {
                selectType = .DiagonalUpRight
                anchorPoint = CGPoint(x: 0.0, y: 0.5)
                position = CGPoint(
                    x: selectStartLoc.x - (boardData.tileWidth / 2),
                    y: selectStartLoc.y - (boardData.tileWidth / 2)
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { $0.coor.row < $1.coor.row })
                
                if let first = selected.first {
                    var maxScale: CGFloat = min(
                        CGFloat(level.dimension - first.coor.row),
                        CGFloat(level.dimension - first.coor.col)
                    )
                    let pointA = getTilePosition(BoardCoordinate(row: 1, col: 1))
                    let pointB = getTilePosition(BoardCoordinate(row: 0, col: 0))
                    let normalizedDist = hypotf(
                        Float(pointA.x - pointB.x),
                        Float(pointA.y - pointB.y)
                    )
                    let dist = CGFloat(normalizedDist) * CGFloat(maxScale)
                    maxScale = CGFloat(dist) / (boardData.tileWidth)
                    scale = min(scale, maxScale)
                }
            }
            else {
                selectType = .DiagonalDownLeft
                anchorPoint = CGPoint(x: 1.0, y: 0.5)
                position = CGPoint(
                    x: selectStartLoc.x + (boardData.tileWidth / 2),
                    y: selectStartLoc.y + (boardData.tileWidth / 2)
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { $0.coor.row > $1.coor.row })

                if let first = selected.first {
                    var maxScale: CGFloat = min(
                        CGFloat(first.coor.row + 1),
                        CGFloat(first.coor.col + 1)
                    )
                    let pointA = getTilePosition(BoardCoordinate(row: 1, col: 1))
                    let pointB = getTilePosition(BoardCoordinate(row: 0, col: 0))
                    let normalizedDist = hypotf(
                        Float(pointA.x - pointB.x),
                        Float(pointA.y - pointB.y)
                    )
                    let dist = CGFloat(normalizedDist) * CGFloat(maxScale)
                    maxScale = CGFloat(dist) / (boardData.tileWidth)
                    scale = min(scale, maxScale)
                }
            }
            
            select.selectType = selectType
            select.node.zRotation = .pi / 4.0
            select.node.xScale = max(1.5,scale)
            select.node.yScale = 1.0
            select.node.anchorPoint = anchorPoint
            select.position = position
        }
        else if diagonalLeft == direction {
            if abs(degrees - 135.0) < abs(degrees - 315.0) {
                selectType = .DiagonalUpLeft
                anchorPoint = CGPoint(x: 1.0, y: 0.5)
                position = CGPoint(
                    x: selectStartLoc.x + (boardData.tileWidth / 2),
                    y: selectStartLoc.y - (boardData.tileWidth / 2)
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { $0.coor.row < $1.coor.row })
                
                if let first = selected.first {
                    var maxScale: CGFloat = min(
                        CGFloat(level.dimension - first.coor.row),
                        CGFloat(first.coor.col + 1)
                    )
                    let pointA = getTilePosition(BoardCoordinate(row: 1, col: 1))
                    let pointB = getTilePosition(BoardCoordinate(row: 0, col: 0))
                    let normalizedDist = hypotf(
                        Float(pointA.x - pointB.x),
                        Float(pointA.y - pointB.y)
                    )
                    let dist = CGFloat(normalizedDist) * CGFloat(maxScale)
                    maxScale = CGFloat(dist) / (boardData.tileWidth)
                    scale = min(scale, maxScale)
                }
            }
            else {
                selectType = .DiagonalDownRight
                anchorPoint = CGPoint(x: 0.0, y: 0.5)
                position = CGPoint(
                    x: selectStartLoc.x - (boardData.tileWidth / 2),
                    y: selectStartLoc.y + (boardData.tileWidth / 2)
                )
                
                let selected = nodes.values
                    .filter{ next in next.isSelected }
                    .sorted(by: { $0.coor.col < $1.coor.col })
                
                if let first = selected.first {
                    var maxScale: CGFloat = min(
                        CGFloat(first.coor.row + 1),
                        CGFloat(level.dimension - first.coor.col)
                    )
                    let pointA = getTilePosition(BoardCoordinate(row: 1, col: 1))
                    let pointB = getTilePosition(BoardCoordinate(row: 0, col: 0))
                    let normalized = hypotf(
                        Float(pointA.x - pointB.x),
                        Float(pointA.y - pointB.y)
                    )
                    let dist = CGFloat(normalized) * CGFloat(maxScale)
                    maxScale = CGFloat(dist) / (boardData.tileWidth)
                    scale = min(scale, maxScale)
                }
            }
            
            select.selectType = selectType
            select.node.zRotation = 7 * .pi / 4.0
            select.node.xScale = max(1.5,scale)
            select.node.yScale = 1.0
            select.node.anchorPoint = anchorPoint
            select.position = position
        }
    }
    
    func endSelection() {
        select.node.removeFromParent()
        select = SelectNode(texture: boardData.selectTexture)
        select.position = CGPoint(x: .min, y: .min)
        
        for child in nodes.values {
            child.isSelected = false
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private

    private func createLetterNode(
        _ coordinate: BoardCoordinate,
        _ character: String
    ) -> LetterNode {
        let node = LetterNode(
            size: boardData.tileWidth,
            letter: character,
            coor: coordinate
        )
        
        node.position = getTilePosition(coordinate)
        node.name = "\(coordinate.col),\(coordinate.row)"
        
        return node
    }
    
    private func getTilePosition(_ coordinate: BoardCoordinate) -> CGPoint {
        return CGPoint(
            x: CGFloat(coordinate.col) * (boardData.tileWidth),
            y: CGFloat(coordinate.row) * (boardData.tileWidth)
        )
    }
}
