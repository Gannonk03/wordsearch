import UIKit
import SpriteKit

class LetterNode {
    var node: SKSpriteNode
    var coor: BoardCoordinate
    var labelNode: SKLabelNode
    var position: CGPoint {
        get {
            return node.position
        }
        set(newPosition) {
            node.position = newPosition
        }
    }
    var isSelected: Bool = false {
        didSet{
            if isSelected {
                labelNode.color = .white
            }
            else {
                labelNode.color = .black
            }
        }
    }
    
    var name: String? {
        get {
            return node.name
        }
        set(newName) {
            node.name = newName; labelNode.name = newName
        }
    }
    
    init(size: CGFloat, letter: String, coor: BoardCoordinate) {
        self.coor = coor
        self.node = SKSpriteNode(
            color: .clear,
            size: CGSize(width: size, height: size)
        )
            
        self.labelNode = SKLabelNode(fontNamed: "AvenirNextCondensed-MediumItalic")
        self.labelNode.colorBlendFactor = 1.0
        self.labelNode.color = .black
        self.labelNode.fontSize = 20.0
        self.labelNode.text = "X"
                
        self.adjustLabelFontSizeToFitRect(
            labelNode: labelNode,
            rect: node.frame.insetBy(dx: 10.0, dy: 10.0)
        )
        
        self.labelNode.text = letter
        self.labelNode.zPosition = 5.0
        
        self.node.addChild(labelNode)
    }
        
    private func adjustLabelFontSizeToFitRect(
        labelNode: SKLabelNode,
        rect: CGRect
    ) {
        let scalingFactor = min(
            rect.width / labelNode.frame.width,
            rect.height / labelNode.frame.height
        )

        labelNode.fontSize *= scalingFactor
        labelNode.position = CGPoint(
            x: rect.midX,
            y: rect.midY - labelNode.frame.height / 2.0
        )
    }
}

