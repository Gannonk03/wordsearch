import SpriteKit
import UIKit

class GameScene: SKScene {
    private var backgroundNode: SKSpriteNode!
    private var levelNode: LevelNode!
    private var selectedTextLabel: SKLabelNode!
    private var playAnother: SKLabelNode!
    private var selectedCoorString: String = ""
    private var panGesture: UIGestureRecognizer!
    private var tapGesture: UIGestureRecognizer!
    
    weak var sceneDelegate: SceneProtocol!
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        setupGameBoard()
        setupGestureRecognizers()
    }
    
    override func update(_ currentTime: TimeInterval) {
        super.update(currentTime)
        
        guard let _ = levelNode.select.node.parent else { return }
        
        var selectedText: String = ""
                
        for child in levelNode.nodes.values.sorted(by: { first, second in
            switch levelNode.select.selectType {
            case .Right:
                return first.coor.col < second.coor.col
            case .Left:
                return first.coor.col > second.coor.col
            case .Up:
                return first.coor.row < second.coor.row
            case .Down:
                return first.coor.row > second.coor.row
            case .DiagonalUpRight, .DiagonalUpLeft:
                return first.coor.row < second.coor.row
            case .DiagonalDownRight, .DiagonalDownLeft:
                return first.coor.row > second.coor.row
            default:
                return first.coor.col < second.coor.col
            }
        }) {
            let selectionNode = levelNode.select.node
            
            if
                child.coor.row == levelNode.select.coor.row &&
                child.coor.col == levelNode.select.coor.col {
                child.isSelected = true
                selectedText += child.labelNode.text ?? ""
                continue
            }
            
            let insetRect = levelNode.select.selectType.isDiagonal
                ? child.node.frame.insetBy(dx: -5.0, dy: -5.0)
                : child.node.frame.insetBy(dx: 8.0, dy: 8.0)
            
            if selectionNode.frame.contains(insetRect) {
                if levelNode.select.selectType.isDiagonal {
                    let startCoor = levelNode.select.coor
                    let xDiff = abs(startCoor.row - child.coor.row)
                    let yDiff = abs(startCoor.col - child.coor.col)
                    
                    if xDiff == yDiff {
                        child.isSelected = true
                        selectedText += child.labelNode.text ?? ""
                        continue
                    }
                    child.isSelected = false
                    continue
                }
                child.isSelected = true
                selectedText += child.labelNode.text ?? ""
                continue
            }
            child.isSelected = false
        }
        
        selectedTextLabel.text = selectedText
    }
            
    @objc func handlePanFrom(recognizer: UIPanGestureRecognizer) {
        if recognizer.state == .began {
            var touchLocation = recognizer.location(in: recognizer.view)
                                    
            touchLocation = convertPoint(fromView: touchLocation)
            
            let boardLoc = levelNode.board.convert(touchLocation, from: self)
            
            guard
                let name = levelNode.board.nodes(at: boardLoc).first?.name,
                let coor = Position.parse(name).first else { return }
            
            levelNode.startSelection(at: BoardCoordinate(row: coor.row, col: coor.column))
            
            return
        }
        if recognizer.state == .changed {
            var translation = recognizer.location(in: recognizer.view!)
            
            translation = convertPoint(fromView: translation)

            levelNode.updateSelection(
                translation: levelNode.board.convert(translation, from: self)
            )
            
            return
        }
        if recognizer.state == .ended {
            if
                let text = selectedTextLabel.text,
                let target = levelNode.level.board.wordLocations.values.first,
                text == target {
                
                let finalScale = CGFloat(target.count)
                
                switch levelNode.select.selectType  {
                case .Up, .Down:
                    levelNode.select.node.yScale = finalScale
                default:
                    levelNode.select.node.xScale = finalScale
                }
                
                view?.removeGestureRecognizer(panGesture)
                
                let spreadAction = SKAction.scale(to: 1.3, duration: 0.25)
                let shrinkAction = SKAction.scale(to: 1.0, duration: 0.5)
                let fadeAction = SKAction.fadeOut(withDuration: 0.25)
                let group = SKAction.group([shrinkAction,fadeAction])
                let popAction = SKAction.sequence([spreadAction,group])
                
                popAction.timingMode = .easeOut
                
                selectedTextLabel.run(popAction) {[weak self] in
                    guard let strongself = self else { return }
                    let fadeAction = SKAction.fadeIn(withDuration: 0.5)
                    let moveAction = SKAction.moveBy(x: 0, y: 2, duration: 0.5)
                    strongself.playAnother.run(SKAction.group([fadeAction,moveAction])) {[weak self] in
                        guard let strongself = self else { return }
                        strongself.view?.addGestureRecognizer(strongself.tapGesture)
                    }
                }

                return
            }
            
            selectedTextLabel.text = ""
            levelNode.endSelection()
        }
    }
    
    @objc func handleTap(recognizer: UIPanGestureRecognizer) {
        self.view?.removeGestureRecognizer(tapGesture)
        self.sceneDelegate?.levelWon(fadeOutTime: 0.3, fadeInDelay: 0.2, fadeInTime: 1.5)
        
        for child in self.children {
            let seq = SKAction.sequence([
                SKAction.fadeOut(withDuration: 0.3),
                SKAction.removeFromParent()
            ])
            
            child.run(seq)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {[weak self] in
            self?.setupGameBoard()
            self?.setupGestureRecognizers()
        }
    }

    //MARK: - Private
        
    private func setupGestureRecognizers() {
        panGesture = UIPanGestureRecognizer(
            target: self,
            action: #selector(handlePanFrom)
        )
        
        tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(handleTap)
        )
        
        self.view?.addGestureRecognizer(panGesture)
    }
    
    private func setupGameBoard() {
        levelNode = LevelNode(sceneDelegate.currentlevel)
        
        selectedTextLabel = SKLabelNode()
        selectedTextLabel.fontColor = UIColor(hexString: "#fd5523")
        selectedTextLabel.fontName = "AvenirNextCondensed-Bold"
        
        playAnother = SKLabelNode()
        playAnother.fontColor = UIColor(hexString: "#37966f")
        playAnother.fontName = "AvenirNextCondensed-MediumItalic"
        playAnother.fontSize = 22.0
        playAnother.alpha = 0.0
        playAnother.text = "Tap to play another"
        
        var center = convertPoint(fromView: CGPoint(
            x: UIScreen.main.bounds.midX,
            y: UIScreen.main.bounds.midY
        ))
        
        center.y += levelNode.level.boardData.boardWidth / 2 + 15.0
        
        selectedTextLabel.position = center
        
        playAnother.position = CGPoint(x: center.x, y: 40.0)

        addChild(levelNode)
        addChild(selectedTextLabel)
        addChild(playAnother)
        
        fadeInLevel()
        
        backgroundColor = UIColor(hexString: "#fffbe6")
    }
    
    private func fadeInLevel() {
        let fadeInAction = SKAction.fadeIn(withDuration: 1.5)
        
        fadeInAction.timingMode = .easeInEaseOut
        
        levelNode.board.run(fadeInAction)
    }
}
