import UIKit

func roundOff(value: CGFloat) -> CGFloat {
    let rounded = abs(floor(value) - value) < abs(ceil(value) - value)
        ? floor(value)
        : ceil(value)
    
    return rounded
}
