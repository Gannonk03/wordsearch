struct Level {
    var dimension: Int { return board.characterGrid.count }
    var board: Grid
    var boardData: GameBoardData
}
