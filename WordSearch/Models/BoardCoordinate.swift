import UIKit

struct BoardCoordinate : Hashable {
    var row: Int
    var col: Int
    
    init(row: Int, col: Int) {
        self.row = row
        self.col = col
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine((31 &* row.hashValue) &+ col.hashValue)
    }
}

func == (lhs: BoardCoordinate, rhs: BoardCoordinate) -> Bool {
    return
        (lhs.row.hashValue == rhs.row.hashValue) &&
        (lhs.col.hashValue == rhs.col.hashValue)
}
