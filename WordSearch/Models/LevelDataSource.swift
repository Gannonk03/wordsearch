import UIKit
import SpriteKit

struct LevelDataSource {
    private let viewWidth: CGFloat
    
    init(playableWidth: CGFloat) {
        self.viewWidth = playableWidth
    }
    
    func getRandomLevel() -> Level {
        let board = Grid.all[
            Int(arc4random_uniform(UInt32(Grid.all.count)))
        ]
        
        let boardData = getBoardData(for: board)
        
        return Level(board: board, boardData: boardData)
    }
    
    //MARK: - Private
    
    private func getBoardData(for board: Grid) -> GameBoardData {
        let dimension = board.characterGrid.count
        
        let width = roundOff(value: viewWidth / CGFloat(dimension))
        
        let boardWidth = width * CGFloat(dimension)
        
        let selectView = SelectView(
            color: UIColor(hexString: "#fd5523"),
            size: CGSize(width: width, height: width)
        )
        
        let texture = SKTexture(image: selectView.takeSnapshot())
                
        return GameBoardData(
            boardWidth: boardWidth,
            tileWidth: width,
            selectTexture: texture
        )
    }
}
