import UIKit
import SpriteKit

struct GameBoardData {
    var boardWidth: CGFloat
    var tileWidth: CGFloat
    var selectTexture: SKTexture
}

enum GameState {
    case Active
    case Completed
}
