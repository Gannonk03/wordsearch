import UIKit
import SpriteKit

protocol SceneProtocol: class {
    func setSelectedText(_ text: String)
    func levelWon(
        fadeOutTime: TimeInterval,
        fadeInDelay: TimeInterval,
        fadeInTime: TimeInterval
    )
    
    var currentlevel: Level! { get }
}

class GameController: UIViewController,
                      SceneProtocol {
    
    private var levelDataSource: LevelDataSource!
    private var scene: GameScene!
    private var wordBankView: WordBank!
    private var selectedTextLabel: UILabel!
    private var skView: SKView { return view as! SKView }
    
    override func loadView() {
        view = SKView(frame: UIScreen.main.bounds)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let playableWidth = view.bounds.width - 30.0
        
        levelDataSource = LevelDataSource(playableWidth: playableWidth)
        currentlevel = levelDataSource.getRandomLevel()
        scene = GameScene(size: view.bounds.size)
        scene.sceneDelegate = self
        
        skView.presentScene(scene)
        
        wordBankView = WordBank(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: playableWidth,
            height: 80.0
        ))
        
        let wordBankContainer = UIView()
        
        wordBankContainer.translatesAutoresizingMaskIntoConstraints = false
        wordBankContainer.addSubview(wordBankView)

        wordBankView.setWord(currentlevel.board.word)
        
        view.addSubview(wordBankContainer)
        
        NSLayoutConstraint.activate([
            wordBankContainer.topAnchor.constraint(
                equalTo: view.safeAreaLayoutGuide.topAnchor,
                constant: 15.0
            ),
            wordBankContainer.widthAnchor.constraint(equalToConstant: playableWidth),
            wordBankContainer.heightAnchor.constraint(equalToConstant: 80.0),
            wordBankContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - SceneProtocol
    
    private(set) var currentlevel: Level!
    
    func levelWon(
        fadeOutTime: TimeInterval,
        fadeInDelay: TimeInterval,
        fadeInTime: TimeInterval
    ) {
        currentlevel = levelDataSource.getRandomLevel()
        wordBankView.setWord(
            currentlevel.board.word,
            fadeOutTime: fadeOutTime,
            fadeInDelay: fadeInDelay,
            fadeInTime: fadeInTime
        )
    }
    
    func setSelectedText(_ text: String) {
        selectedTextLabel.text = text
        selectedTextLabel.sizeToFit()
    }
}


